import App from 'next/app';
import Head from 'next/head';
import React from 'react';

export default class MyApp extends App {
	static async getInitialProps({ Component, ctx }) {
		let pageProps = {};

		if (Component.getInitialProps) {
			pageProps = await Component.getInitialProps(ctx);
		}

		return { pageProps };
	}

	render() {
		const { Component, pageProps } = this.props;

		return (
			<React.Fragment>
				<Head>
					<title>Cultura Purpura</title>
					<meta charSet='UTF-8' />
					<meta name='description' content='EndGam Gaming Magazine Template' />
					<meta name='keywords' content='endGam,gGaming, magazine, html' />
					<meta name='viewport' content='width=device-width, initial-scale=1.0' />

					<link href='img/favicon.ico' rel='shortcut icon' />

					<link
						href='https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i'
						rel='stylesheet'
					/>

					<link rel='stylesheet' href='/static/css/bootstrap.min.css' />
					<link rel='stylesheet' href='/static/css/font-awesome.min.css' />
					<link rel='stylesheet' href='/static/css/slicknav.min.css' />
					<link rel='stylesheet' href='/static/css/owl.carousel.min.css' />
					<link rel='stylesheet' href='/static/css/magnific-popup.css' />
					<link rel='stylesheet' href='/static/css/animate.css' />

					<link rel='stylesheet' href='/static/css/style.css' />

					<script src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js' />
					<script src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js' />
				</Head>
				<Component {...pageProps} />
			</React.Fragment>
		);
	}
}
