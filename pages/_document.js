import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
	static async getInitialProps(ctx) {
		const initialProps = await Document.getInitialProps(ctx);
		return { ...initialProps };
	}

	render() {
		return (
			<Html>
				<Head />
				<body>
					<Main />
					<NextScript />
					<script type='text/javascript' src='/static/js/jquery-3.2.1.min.js' />
					<script type='text/javascript' src='/static/js/bootstrap.min.js' />
					<script type='text/javascript' src='/static/js/jquery.slicknav.min.js' />
					<script type='text/javascript' src='/static/js/owl.carousel.min.js' />
					<script type='text/javascript' src='/static/js/jquery.sticky-sidebar.min.js' />
					<script type='text/javascript' src='/static/js/jquery.magnific-popup.min.js' />
					<script type='text/javascript' src='/static/js/main.js' />
				</body>
			</Html>
		);
	}
}

export default MyDocument;
