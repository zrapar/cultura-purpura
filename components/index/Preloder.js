import React from 'react';

const Preloder = () => {
	return (
		<div id='preloder'>
			<div className='loader' />
		</div>
	);
};

export default Preloder;
