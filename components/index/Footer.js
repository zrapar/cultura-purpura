import React from 'react';

const Footer = () => {
	return (
		<footer className='footer-section'>
			<div className='container'>
				{/* <div className='footer-left-pic'>
					<img src='/static/img/footer-left-pic.png' alt='' />
				</div>
				<div className='footer-right-pic'>
					<img src='/static/img/footer-right-pic.png' alt='' />
				</div> */}
				<a href='#' className='footer-logo'>
					<img src='/static/img/logocultura-white.png' alt='' />
				</a>
				<ul className='main-menu footer-menu'>
					<li>
						<a href='#'>¿Quiénes somos?</a>
					</li>
					<li>
						<a href='#'>Noticias</a>
					</li>
					<li>
						<a href='#'>Editoriales</a>
					</li>
					<li>
						<a href='#'>Eventos</a>
					</li>
					<li>
						<a href='#'>Reseñas</a>
					</li>
					<li>
						<a href='#'>Entrevistas</a>
					</li>
					<li>
						<a href='#'>Podcasts</a>
					</li>
					<li>
						<a href='#'>Videos</a>
					</li>
					<li>
						<a href='#'>Contacto</a>
					</li>
				</ul>
				<div className='footer-social d-flex justify-content-center'>
					<a href='#'>
						<i className='fa fa-pinterest' />
					</a>
					<a href='#'>
						<i className='fa fa-facebook' />
					</a>
					<a href='#'>
						<i className='fa fa-twitter' />
					</a>
					<a href='#'>
						<i className='fa fa-dribbble' />
					</a>
					<a href='#'>
						<i className='fa fa-behance' />
					</a>
				</div>
				<div className='copyright'>
					<a href=''>Colorlib</a> 2018 @ All rights reserved
				</div>
			</div>
		</footer>
	);
};

export default Footer;
