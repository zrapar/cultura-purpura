import React from 'react';

const Carousel = () => {
	return (
		<section className='hero-section overflow-hidden'>
			<div className='hero-slider owl-carousel'>
				<div
					className='hero-item set-bg d-flex align-items-center justify-content-center text-center'
					data-setbg='/static/img/slider-bg-1.jpg'
				>
					<div className='container'>
						<h2>Game on!</h2>
						<p>
							Fusce erat dui, venenatis et erat in, vulputate dignissim lacus. Donec vitae tempus dolor,<br />sit
							amet elementum lorem. Ut cursus tempor turpis.
						</p>
						<a href='#' className='site-btn'>
							Leer más <img src='/static/img/icons/double-arrow.png' alt='#' />
						</a>
					</div>
				</div>
				<div
					className='hero-item set-bg d-flex align-items-center justify-content-center text-center'
					data-setbg='/static/img/slider-bg-2.jpg'
				>
					<div className='container'>
						<h2>Game on!</h2>
						<p>
							Fusce erat dui, venenatis et erat in, vulputate dignissim lacus. Donec vitae tempus dolor,<br />sit
							amet elementum lorem. Ut cursus tempor turpis.
						</p>
						<a href='#' className='site-btn'>
							Leer más <img src='/static/img/icons/double-arrow.png' alt='#' />
						</a>
					</div>
				</div>
			</div>
		</section>
	);
};

export default Carousel;
