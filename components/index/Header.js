import React from 'react';
import Link from 'next/link';

const Header = () => {
	const onToggleOpen = (e) => {
		if (e.currentTarget.src.includes('white')) {
			e.currentTarget.src = '/static/img/logocultura-color.png';
		} else {
			e.currentTarget.src = '/static/img/logocultura-white.png';
		}
	};

	return (
		<header className='header-section'>
			<div className='header-warp'>
				<div className='header-social d-flex justify-content-end'>
					<p>Culturízate en: </p>
					<a href='#'>
						<i className='fa fa-facebook' />
					</a>
					<a href='#'>
						<i className='fa fa-twitter' />
					</a>
					<a href='#'>
						<i className='fa fa-instagram' />
					</a>
				</div>
				<div className='header-bar-warp d-flex'>
					<Link href='/'>
						<a className='site-logo'>
							<img
								onMouseOver={onToggleOpen}
								onMouseOut={onToggleOpen}
								src='/static/img/logocultura-white.png'
								alt='Logo Cultura'
							/>
						</a>
					</Link>
					<nav className='top-nav-area w-100 navbar-center'>
						{/* <div className='user-panel'>
							<a href=''>Login</a> / <a href=''>Register</a>
						</div> */}

						<ul className='main-menu primary-menu'>
							<li>
								<a href='#'>¿Quiénes somos?</a>
							</li>
							<li>
								<a href='#'>Noticias</a>
							</li>
							<li className='dropdown'>
								<a href='#'>Editoriales</a>
								<ul className='sub-menu'>
									<li>
										<a href='#'>Sub páginas</a>
									</li>
								</ul>
							</li>
							<li className='dropdown'>
								<a href='#'>Eventos</a>
								<ul className='sub-menu'>
									<li>
										<a href='#'>Sub páginas</a>
									</li>
								</ul>
							</li>
							<li className='dropdown'>
								<a href='#'>Reseñas</a>
								<ul className='sub-menu'>
									<li>
										<a href='#'>Sub páginas</a>
									</li>
								</ul>
							</li>
							<li>
								<a href='#'>Entrevistas</a>
							</li>
							<li>
								<a href='#'>Podcasts</a>
							</li>
							<li>
								<a href='#'>Videos</a>
							</li>
							<li>
								<a href='#'>Contacto</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
	);
};

export default Header;
