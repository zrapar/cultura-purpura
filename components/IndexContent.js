import React from 'react';
import Preloder from './index/Preloder';
import Header from './index/Header';
import Carousel from './index/Carousel';
import StarredPost from './index/StarredPost';
import Blog from './index/Blog';
import VideoSection from './index/VideoSection';
import Featured from './index/Featured';
import Newsletters from './index/Newsletters';
import Footer from './index/Footer';

const IndexContent = () => {
	return (
		<React.Fragment>
			<Preloder />
			<Header />
			<Carousel />
			<StarredPost />
			<Blog />
			<VideoSection />
			<Featured />
			<Newsletters />
			<Footer />
		</React.Fragment>
	);
};

export default IndexContent;
